package com.example.testGyT;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestGyTApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestGyTApplication.class, args);
	}

}
