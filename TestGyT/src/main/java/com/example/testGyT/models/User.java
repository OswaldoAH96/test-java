/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.testGyT.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author oswaldo
 */
@Entity(name = "users")
@Table(name = "users")
public class User implements Serializable {

    @Id
    @Column(name = "dpi", nullable = false)
    private String dpi;
    @Column(name = "name")
    private String name;
    @Column(name = "points", columnDefinition = "integer default 0")
    private int points;
    
    @OneToMany(mappedBy = "user")
    private List<UserAwards> awards;

    public String getDpi() {
        return dpi;
    }

    public void setDpi(String dpi) {
        this.dpi = dpi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public List<UserAwards> getAwards() {
        return awards;
    }

    public void setAwards(List<UserAwards> awards) {
        this.awards = awards;
    }

    @Override
    public String toString() {
        return "User{" + "dpi=" + dpi + ", name=" + name + '}';
    }

}
