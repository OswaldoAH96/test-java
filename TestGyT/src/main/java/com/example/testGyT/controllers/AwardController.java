/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.testGyT.controllers;

import com.example.testGyT.dao.AwardDao;
import com.example.testGyT.models.Award;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author oswaldo
 */
@RestController
public class AwardController {
    @Autowired
    private AwardDao awardDao;
    
    @RequestMapping(value = "api/award/", method = RequestMethod.GET)
    public List<Award> getAwards() {
        return awardDao.listAwards();
    }
    
    @RequestMapping(value = "api/award/", method = RequestMethod.POST)
    public boolean postAward(@RequestBody Award award) {
        return awardDao.createAward(award);
    }
    
}
