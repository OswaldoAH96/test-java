package com.example.testGyT.controllers;

import com.example.testGyT.dao.UserDao;
import com.example.testGyT.models.User;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author oswaldo
 */
@RestController
public class UserController {

    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "api/user/", method = RequestMethod.GET)
    public List<User> getUsers() {
        return userDao.getUsers();
    }

    @RequestMapping(value = "api/user/", method = RequestMethod.POST)
    public boolean postUser(@RequestBody User user) {
        return userDao.createUser(user);
    }

    @RequestMapping(value = "api/user/{dpi}", method = RequestMethod.PUT)
    public User putPointsUser(@PathVariable String dpi, @RequestBody Map<String, Object> data) {
        
        return userDao.updatePoints(Integer.parseInt(data.get("points").toString()), dpi);
    }

}
