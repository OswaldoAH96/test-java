/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.testGyT.controllers;

import com.example.testGyT.dao.AwardDao;
import com.example.testGyT.dao.UserDao;
import com.example.testGyT.models.Award;
import com.example.testGyT.models.User;
import com.example.testGyT.models.UserAwards;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author oswaldo
 */
@RestController
public class userAwardController {
    @Autowired
    private AwardDao awardDao;
    @Autowired
    private UserDao userDao;
    
    @RequestMapping(value = "api/award/{dpi}", method = RequestMethod.POST)
    public UserAwards assignAward(@PathVariable String dpi, @RequestBody Map<String,Long> data) {
        User user = userDao.searchDpi(dpi);
        Award award = awardDao.searchId(data.get("id"));
        return awardDao.assignAwardToUser(user,award);
    }
    @RequestMapping(value = "api/award/{dpi}", method = RequestMethod.GET)
    public List<Award> getAwards(@PathVariable String dpi) {
        System.out.println(awardDao.getListAwards(dpi));
        return awardDao.getListAwards(dpi);
    }
    
}
