/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.testGyT.dao;

import com.example.testGyT.models.User;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author oswaldo
 */
@Repository
@Transactional
public class UserDaoImp implements UserDao {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    @Transactional
    public List<User> getUsers() {
        String query = "FROM users";
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public boolean createUser(User user) {
        String query = "FROM users WHERE dpi = :dpi";
        if(entityManager.createQuery(query).setParameter("dpi", user.getDpi()).getResultList().size() >0)
            return false;
        entityManager.merge(user);
        return true;
    }

    @Override
    public User updatePoints(int points, String dpi) {
        User user = entityManager.find(User.class, dpi);
        user.setPoints(points);
        return entityManager.merge(user);
    }

    @Override
    public User searchDpi(String dpi) {
        return entityManager.find(User.class, dpi);
    }

}
