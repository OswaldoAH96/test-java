/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.testGyT.dao;

import com.example.testGyT.models.Award;
import com.example.testGyT.models.User;
import com.example.testGyT.models.UserAwards;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author oswaldo
 */
@Repository
@Transactional
public class AwardDaoImp implements AwardDao {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public boolean createAward(Award award) {
        award.setActive(true);
        entityManager.merge(award);
        return true;
    }

    @Override
    @Transactional
    public List<Award> listAwards() {
        String query = "FROM awards";
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public Award searchId(Long id) {
        return entityManager.find(Award.class, id);
    }

    @Override
    public UserAwards assignAwardToUser(User user, Award award) {
        UserAwards detail = new UserAwards();
        detail.setUser(user);
        detail.setAward(award);
        detail.setRedeemed(false);
        return entityManager.merge(detail);
    }

    @Override
    public List<Award> getListAwards(String dpi) {
        User user = entityManager.find(User.class, dpi);
        return user.getAwards().stream().map(UserAwards::getAward).sequential().collect(Collectors.toList());
    }

}
