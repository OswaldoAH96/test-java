/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.testGyT.dao;

import com.example.testGyT.models.Award;
import com.example.testGyT.models.User;
import com.example.testGyT.models.UserAwards;
import java.util.List;

/**
 *
 * @author oswaldo
 */
public interface AwardDao {

    public boolean createAward(Award award);

    public List<Award> listAwards();

    public Award searchId(Long id);

    public UserAwards assignAwardToUser(User user, Award award);

    public List<Award> getListAwards(String dpi);
    
}
