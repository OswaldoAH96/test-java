/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.testGyT.dao;

import com.example.testGyT.models.User;
import java.util.List;

/**
 *
 * @author oswaldo
 */
public interface UserDao {
    public List<User> getUsers();

    public boolean createUser(User user);

    public User updatePoints(int points, String dpi);

    public User searchDpi(String dpi);
    
}
