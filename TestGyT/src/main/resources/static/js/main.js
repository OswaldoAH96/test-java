/**
 * Función para registrar un nuevo usuario con dpi como PK
 */
async function register() {
    const dpi = document.getElementById("dpi");
    const name = document.getElementById("name");
    if (dpi.value.length != 0 && name.value.length != 0) {
        let text = "";
        let icon = "";
        const response = await fetch('api/user/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                'dpi': dpi.value,
                'name': name.value
            })
        });
        if (response.status == 200) {
            const user = await response.json();
            if (user) {
                title = "Se ha registrado correctamente";
                icon = "success"
                text = `se registró correctamente: Nombre:${name.value}--DPI:${dpi.value}`;
            } else {
                title = "Información";
                icon = "warning"
                text = `El número de DPI:" + ${dpi.value} ya fue registrado anteriormente`;
            }
            console.log(user);
        } else {
            icon = "error";
            title = "Ha ocurrido un error";
        }

        Swal.fire({
            position: 'center',
            icon: icon,
            title: title,
            text: text,
            showConfirmButton: true,
        });
        dpi.value = "";
        name.value = "";
    }
}
/**
 * Obtener la lista de usuarios registrados
 * Se crean los elementos necesarios de la tabla
 * Se crean los botones para editar o asignar puntos a los usuarios
 */
async function userList() {
    const response = await fetch('api/user/', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json'
        },
    });
    const users = await response.json();
    const table = document.querySelector("#userList tbody");
    table.innerHTML = "";
    users.forEach(user => {
        const tr = document.createElement("tr");
        const tdDpi = document.createElement("td");
        const tdName = document.createElement("td");
        const tdPoints = document.createElement("td");
        const button = document.createElement("button");
        tdDpi.innerText = user.dpi;
        tdName.innerText = user.name;
        button.textContent = user.points;
        button.style.width = "100%";
        button.classList.add("btn", "btn-outline-light");
        button.addEventListener("click", function() {
            assingPoints(user, button);
        });
        tdPoints.appendChild(button);
        tr.appendChild(tdDpi);
        tr.appendChild(tdName);
        tr.appendChild(tdPoints);
        table.appendChild(tr);
    });
}

/**
 * Función para actualizar los puntos de un usuario
 * Se emplea sweetalert2 para obtener deplegar un alert con un input
 * Posteriomente se valida si fue aceptado 
 * En caso de que no fuera se le envía un mensaje
 * De lo contrario hace la llamada a la API para poder actualizar los puntos del usuario
 * @param {*} user 
 * @param {*} btn 
 */
async function assingPoints(user, btn) {
    const inputValue = ""
    const {
        value: points
    } = await Swal.fire({
        title: 'Asignar puntos',
        input: 'text',
        inputLabel: 'Puntos a asignar',
        inputValue: inputValue,
        showCancelButton: true,
        inputValidator: (value) => {
            if (!value || value < 0) {
                return 'Ingresa un número positivo mayor a 0'
            }
        }
    });
    if (points) {
        await fetch(`api/user/${user.dpi}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                'points': points
            })
        }).then(response => response.json()).then(data => {
            Swal.fire({
                icon: 'success',
                title: 'Actualización de puntos',
                text: `Se actualizaron los puntos del usuario con DPI: ${data.dpi}`,
                showConfirmButton: true,
            });
            btn.textContent = data.points;
        }).catch(response => {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error',
                text: 'No fue posible actulizar los puntos.',
                showConfirmButton: false,
            });
        });
    }
}

async function createAward() {
    const name = document.getElementById("awardName");
    const desc = document.getElementById("awardDescription");
    const points = document.getElementById("awardPoints");
    if (desc.value.length != 0 && name.value.length != 0 && points.value.length != 0) {
        let text = "";
        let icon = "";
        const response = await fetch('api/award/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                'name': name.value,
                'description': desc.value,
                'points': points.value
            })
        });
        if (response.status == 200) {
            const user = await response.json();
            if (user) {
                title = "Se ha registrado correctamente";
                icon = "success"
            }
        } else {
            icon = "error";
            title = "Ha ocurrido un error";
        }

        Swal.fire({
            position: 'center',
            icon: icon,
            title: title,
            text: text,
            showConfirmButton: true,
        });
    }
}


async function usersAndAwards() {
    const userSelect = document.getElementById("userSelect");
    const awardSelect = document.getElementById("awardSelect");
    userSelect.innerHTML = "";
    awardSelect.innerHTML = "";
    let response = await fetch('api/user/', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json'
        },
    });
    const users = await response.json();
    users.forEach(user => {
        const option = document.createElement("option");
        option.value = user.dpi;
        option.text = `DPI: ${user.dpi} - Nombre: ${user.name}`;
        userSelect.appendChild(option);
    });
    response = await fetch('api/award/', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json'
        },
    });
    const awards = await response.json();
    awards.forEach(award => {
        const option = document.createElement("option");
        option.value = award.id;
        option.text = `Nombre: ${award.name}`;
        awardSelect.appendChild(option);
    });
}

async function assignAwards() {
    const user = document.getElementById("userSelect");
    const award = document.getElementById("awardSelect");
    const response = await fetch(`api/award/${user.value}/`, {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json'
        },
        body: JSON.stringify({
            'id': award.value,
        })
    })
    let text = "";
    let icon = "";
    let title = "";
    if (response.status == 200) {
        const user = await response.json();
        if (user) {
            title = "Asignado correctamente";
            icon = "success"
        }
    } else {
        icon = "error";
        title = "Ha ocurrido un error";
    }

    Swal.fire({
        position: 'center',
        icon: icon,
        title: title,
        text: text,
        showConfirmButton: true,
    });
}
async function verify() {
    const acc = document.getElementById("acc");
    acc.classList.add("d-none");
    let user = document.getElementById("dpiAwards");
    const response = await fetch(`api/award/${user.value}`, {
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json'
        },
    })
    if (response.ok) {
        const data = await response.json();
        if (data.length != 0) {
            acc.classList.remove("d-none");
            const accord = document.getElementById("accordionFlushExample");
            accord.innerHTML = "";
            data.forEach(e => {
                if (e.active) {
                    const div = document.createElement("div");
                    const h2 = document.createElement("h2");
                    const button = document.createElement("button");
                    const div1 = document.createElement("div");
                    const div2 = document.createElement("div");
                    const div3 = document.createElement("div");
                    const button1 = document.createElement("button");
                    div.classList.add("accordion-item");
                    h2.classList.add("accordion-header");
                    h2.id = `flush-heading${e.id}`;
                    button.classList.add("accordion-button", "collapsed");
                    button.type = "button";
                    button.setAttribute("data-bs-toggle", "collapse");
                    button.setAttribute("data-bs-target", `#flush-collapse${e.id}`);
                    button.setAttribute("aria-expanded", "false");
                    button.setAttribute("aria-controls", `flush-collapse${e.id}`);
                    button.textContent = e.name;
                    h2.appendChild(button);
                    div.appendChild(h2)
                    accord.appendChild(div)
                    div1.id = `flush-collapse${e.id}`
                    div1.classList.add("accordion-collapse", "collapse", )
                    div1.setAttribute("aria-labelledby", `flush-heading${e.id}`)
                    div1.setAttribute("data-bs-parent", "#accordionFlushExample")
                    div2.classList.add("accordion-body")
                    div2.innerText = e.description
                    div3.classList.add("text-center");
                    button1.classList.add("btn", "btn-outline-primary")
                    button1.textContent = "Canjear";
                    div3.appendChild(button1);
                    div2.appendChild(div3)
                    div1.appendChild(div2)
                    accord.appendChild(div1)
                }
            });
        }
        // <div class="text-center"><button class="btn btn-outline-primary">Canjear</button></div>
    }
}